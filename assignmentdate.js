var assignmentDateString = '01/14/2018'.split('/');
var assignmentDate = new Date(assignmentDateString[2],assignmentDateString[0],assignmentDateString[1]);
var dueDate = assignmentDate;
dueDate.setDate(dueDate.getDate()+7);
dueDate.setMonth(dueDate.getMonth())
//https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getMonth
var month = (new Intl.DateTimeFormat('en-US', {month:'long'}).format(dueDate));
document.write(`<time datetime="${dueDate.getYear()+1900}-${dueDate.getMonth()}-${dueDate.getDate()}">${month} ${dueDate.getDate()}, ${dueDate.getYear()+1900}</time>`);